package microgram.impl.srv.java;

import static microgram.api.java.Result.error;
import static microgram.api.java.Result.ok;
import static microgram.api.java.Result.ErrorCode.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import microgram.api.Profile;
import microgram.api.java.Posts;
import microgram.api.java.Profiles;
import microgram.api.java.Result;
import microgram.impl.srv.rest.RestResource;

public class JavaProfiles extends RestResource implements Profiles {
	
	private Map<String, Profile> users = new ConcurrentHashMap<>();
	private Map<String, Set<String>> followers = new ConcurrentHashMap<>();
	private Map<String, Set<String>> following = new ConcurrentHashMap<>();
	
	private Posts c;
	
	public JavaProfiles(Posts c) {
		this.c = c;
	}

	@Override
	public Result<Profile> getProfile(String userId) {
		Profile res = users.get(userId);
		if (res == null)
			return error(NOT_FOUND);

		res.setFollowers(followers.get(userId).size());
		res.setFollowing(following.get(userId).size());
		return ok(res);
	}

	@Override
	public Result<Void> createProfile(Profile profile) {
		Profile res = users.putIfAbsent(profile.getUserId(), profile);
		if (res != null)
			return error(CONFLICT);
		followers.put(profile.getUserId(), new HashSet<String>());
		following.put(profile.getUserId(), new HashSet<String>());
		return ok();
	}

	@Override
	public Result<Void> deleteProfile(String userId) {
        Profile p = users.remove(userId);
        if (p == null)
            return Result.error(Result.ErrorCode.NOT_FOUND);

        Set<String> myFollowees = following.remove(userId);
        Set<String> myFollowers = followers.remove(userId);

        for (String followee : myFollowees)
            followers.get(followee).remove(userId);

        for (String follower : myFollowers)
            following.get(follower).remove(userId);
		c.deleteActivity(userId);
        return Result.ok();
	}

	@Override
	public Result<List<Profile>> search(String prefix) {
		return ok(users.values().stream().filter(p -> p.getUserId().startsWith(prefix)).collect(Collectors.toList()));
	}

	@Override
	public Result<Void> follow(String userId1, String userId2, boolean isFollowing) {
		Set<String> s1 = following.get(userId1);
		Set<String> s2 = followers.get(userId2);

		if (s1 == null || s2 == null)
			return error(NOT_FOUND);

		if (isFollowing) {
			boolean added1 = s1.add(userId2), added2 = s2.add(userId1);
			if (!added1 || !added2)
				return error(CONFLICT);
		} else {
			boolean removed1 = s1.remove(userId2), removed2 = s2.remove(userId1);
			if (!removed1 || !removed2)
				return error(NOT_FOUND);
		}
		following.put(userId1, s1);
		followers.put(userId2, s2);
        users.get(userId1).setFollowing(s1.size());
        users.get(userId2).setFollowers(s2.size());
		return ok();
	}

	@Override
	public Result<Boolean> isFollowing(String userId1, String userId2) {

		Set<String> s1 = following.get(userId1);
		Set<String> s2 = followers.get(userId2);

		if (s1 == null || s2 == null)
			return error(NOT_FOUND);
		else
			return ok(s1.contains(userId2) && s2.contains(userId1));
	}

	@Override
	public Result<List<String>> followers(String userId) {
		Profile res = users.get(userId);
		if (res == null)
			return error(NOT_FOUND);
		return ok(new ArrayList<String>(following.get(userId)));
	}

	@Override
	public Result<Void> updateProfile(String userId, Profile p) {
		Profile res = users.put(userId, p);
		if (res == null)
			return error(NOT_FOUND);
		return ok();
	}
}
