package microgram.impl.srv.java;

import static microgram.api.java.Result.error;
import static microgram.api.java.Result.ok;
import static microgram.api.java.Result.ErrorCode.CONFLICT;
import static microgram.api.java.Result.ErrorCode.NOT_FOUND;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import microgram.api.Post;
import microgram.api.Profile;
import microgram.api.java.Media;
import microgram.api.java.Posts;
import microgram.api.java.Profiles;
import microgram.api.java.Result;
import microgram.impl.clt.java.ClientFactory;
import utils.Hash;

public class JavaPosts implements Posts {

	private Map<String, Post> posts = new ConcurrentHashMap<>();
	private Map<String, Set<String>> likes = new ConcurrentHashMap<>();
	private Map<String, Set<String>> userPosts = new ConcurrentHashMap<>();

	private Profiles c;
	private Media m;

	public JavaPosts(URI profilesUri, URI mediaUri) {
		c = ClientFactory.getProfilesClient(profilesUri);
		m = ClientFactory.getMediaClient(mediaUri);
	}

	@Override
	public Result<Post> getPost(String postId) {
		Post res = posts.get(postId);
		if (res != null)
			return ok(res);
		else
			return error(NOT_FOUND);
	}

	@Override
	public Result<Void> deletePost(String postId) {
		try {
			Post p = posts.remove(postId);
			if (p == null)
				return Result.error(NOT_FOUND);
			likes.remove(postId);
			m.delete(p.getMediaUrl().substring(p.getMediaUrl().lastIndexOf('/') + 1));
			Set<String> singleUserPosts = userPosts.get(p.getOwnerId());
			singleUserPosts.remove(postId);
			return ok();
		} catch (Exception e) {
			e.printStackTrace();
			return Result.error(Result.ErrorCode.INTERNAL_ERROR);
		}
	}

	@Override
	public Result<String> createPost(Post post) {
		String postId = Hash.of(post.getOwnerId(), post.getMediaUrl());
		if (posts.putIfAbsent(postId, post) == null) {
			likes.put(postId, new HashSet<>());
			Set<String> pots = userPosts.get(post.getOwnerId());
			if (pots == null) {
				pots = new LinkedHashSet<>();
				pots.add(postId);
				userPosts.put(post.getOwnerId(), pots);
			}
			Profile p = c.getProfile(post.getOwnerId()).value();
			int f = p.getPosts();
			p.setPosts(f + 1);
			c.updateProfile(post.getOwnerId(), p);
		}
		return ok(postId);
	}

	@Override
	public Result<Void> like(String postId, String userId, boolean isLiked) {

		Set<String> res = likes.get(postId);
		if (res == null)
			return error(NOT_FOUND);
		if (isLiked) {
			if (!res.add(userId))
				return error(CONFLICT);
		} else {
			if (!res.remove(userId))
				return error(NOT_FOUND);
		}
		Post p = getPost(postId).value();
		p.setLikes(res.size());
		posts.put(postId, p);
		return ok();
	}

	@Override
	public Result<Boolean> isLiked(String postId, String userId) {
		Set<String> res = likes.get(postId);
		if (res != null)
			return ok(res.contains(userId));
		else
			return error(NOT_FOUND);
	}

	@Override
	public Result<List<String>> getPosts(String userId) {
        return Result.error(Result.ErrorCode.NOT_IMPLEMENTED);
	}

	@Override
	public Result<List<String>> getFeed(String userId) {
		List<String> followers = null;
		followers = c.followers(userId).value();
		List<String> res = new ArrayList<>();
		for (String s : followers) {
			res.addAll(getPosts(s).value());
		}
		if (!res.isEmpty())
			return ok(res);
		else
			return error(NOT_FOUND);
	}

	@Override
	public Result<Void> deleteActivity(String userId) {
		try {
			Set<String> postsToRemove = userPosts.remove(userId);
			if (postsToRemove != null) {
				for (String s : postsToRemove) {
					Post p = posts.remove(s);
					likes.remove(s);
					m.delete(p.getMediaUrl().substring(p.getMediaUrl().lastIndexOf('/') + 1));
				}
			}
			for (String post : likes.keySet()) {
				if (likes.get(post).remove(userId)) {
					posts.get(post).setLikes(likes.get(post).size());
				}
			}
			return Result.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return Result.error(Result.ErrorCode.INTERNAL_ERROR);
		}
	}

	@Override
	public Result<List<String>> getPostsInternal(String userId) {
        Set<String> res = userPosts.get(userId);
        return Result.ok(res != null ? new ArrayList<>(res) : new ArrayList<>());
	}
}