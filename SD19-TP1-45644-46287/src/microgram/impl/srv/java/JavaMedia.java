package microgram.impl.srv.java;

import static microgram.api.java.Result.ok;
import static microgram.api.java.Result.ErrorCode.INTERNAL_ERROR;
import static microgram.api.java.Result.ErrorCode.NOT_FOUND;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import microgram.api.java.Media;
import microgram.api.java.Result;
import microgram.api.java.Result.ErrorCode;
import utils.Hash;

public class JavaMedia implements Media {

	protected Map<String, File> media = new ConcurrentHashMap<>();

	private static final String MEDIA_EXTENSION = ".jpg";
	private static final String ROOT_DIR = "/tmp/microgram/";
	private static final String PATH = "/media";
	
	final String baseURI;
	
	public JavaMedia(String baseURI) {
		this.baseURI = baseURI + PATH;
		new File(ROOT_DIR).mkdirs();
	}
	
	@Override
	public Result<String> upload(byte[] bytes) {
		String id = Hash.of(bytes);
		File filename = new File(ROOT_DIR + id + MEDIA_EXTENSION);
		if (!media.containsKey(id)) {
			File f = new File(id);
			try {
				Files.write(filename.toPath(), bytes);
			} catch (IOException e) {
				e.printStackTrace();
				return Result.error(INTERNAL_ERROR);
			}
			media.put(id, f);
		} else
			return Result.error(ErrorCode.CONFLICT);
		return ok(baseURI + "/" + id);
	}

	@Override
	public Result<byte[]> download(String id) {
		try {
			File filename = new File(ROOT_DIR + id + MEDIA_EXTENSION);
			if (filename.exists())
				return Result.ok(Files.readAllBytes(filename.toPath()));
			else
				return Result.error(NOT_FOUND);
		} catch (Exception x) {
			x.printStackTrace();
			return Result.error(INTERNAL_ERROR);
		}
	}

	@Override
	public Result<Void> delete(String id) {
		try {
			File filename = new File(ROOT_DIR + id + MEDIA_EXTENSION);
            if (!filename.exists())
                return Result.error(Result.ErrorCode.NOT_FOUND);
            Files.delete(filename.toPath());
            return Result.ok();
		} catch (Exception x) {
			x.printStackTrace();
			return Result.error(INTERNAL_ERROR);
		}
	}
}
