package microgram.impl.srv.soap;

import java.net.URI;
import java.util.List;

import javax.jws.WebService;

import microgram.api.Profile;
import microgram.api.java.Profiles;
import microgram.api.soap.MicrogramException;
import microgram.api.soap.SoapProfiles;
import microgram.impl.srv.java.ProfilesDistributionCoordinator;

@WebService(serviceName = SoapProfiles.NAME, targetNamespace = SoapProfiles.NAMESPACE, endpointInterface = SoapProfiles.INTERFACE)
public class ProfilesWebService extends SoapService implements SoapProfiles {

	final Profiles impl;

	public ProfilesWebService(String serverURI, URI[] profilesURIs, URI postsUri) {
		this.impl = new ProfilesDistributionCoordinator(serverURI, profilesURIs, postsUri);
	}

	@Override
	public Profile getProfile(String userId) throws MicrogramException {
		return super.resultOrThrow(impl.getProfile(userId));
	}

	@Override
	public void createProfile(Profile profile) throws MicrogramException {
		super.resultOrThrow(impl.createProfile(profile));
	}

	@Override
	public void deleteProfile(String userId) throws MicrogramException {
		super.resultOrThrow(impl.deleteProfile(userId));
	}

	@Override
	public List<Profile> search(String prefix) throws MicrogramException {
		return super.resultOrThrow(impl.search(prefix));
	}

	@Override
	public void follow(String userId1, String userId2, boolean isFollowing) throws MicrogramException {
		super.resultOrThrow(impl.follow(userId1, userId2, isFollowing));
	}

	@Override
	public boolean isFollowing(String userId1, String userId2) throws MicrogramException {
		return super.resultOrThrow(impl.isFollowing(userId1, userId2));
	}

	@Override
	public List<String> followers(String userId) throws MicrogramException {
		return super.resultOrThrow(impl.followers(userId));
	}

	@Override
	public void updateProfile(String userId, Profile profile) throws MicrogramException {
		super.resultOrThrow(impl.updateProfile(userId, profile));
	}
}
